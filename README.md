<div id="top"></div>

<div align="center">
  <a href="https://gitlab.com/marekstreich/cv">
    <img src="docs/images/logo-400.png" alt="Logo" width="80" height="80">
  </a>

  <h3 align="center">CV Generator</h3>

  <p align="center">
    A simple React application for creating a resume.
    <br />
    <a href="https://gitlab.com/marekstreich/cv/-/blob/master/README.md"><strong>Explore the docs »</strong></a>
    <br />
    <br />
    <a href="https://gitlab.com/marekstreich/cv/-/issues">Report Bug</a>
    ·
    <a href="https://gitlab.com/marekstreich/cv/-/issues">Request Feature</a>
  </p>
</div>

<details>
  <summary>Table of Contents</summary>
  <ol>
    <li>
      <a href="#about-the-project">About The Project</a>
      <ul>
        <li><a href="#built-with">Built With</a></li>
      </ul>
    </li>
    <li>
      <a href="#getting-started">Getting Started</a>
      <ul>
        <li><a href="#prerequisites">Prerequisites</a></li>
        <li><a href="#installation">Installation</a></li>
      </ul>
    </li>
    <li><a href="#usage">Usage</a></li>
    <li><a href="#roadmap">Roadmap</a></li>
    <li><a href="#contributing">Contributing</a></li>
    <li><a href="#contact">Contact</a></li>
  </ol>
</details>

## About The Project

<img src="docs/images/screenshot-1.png" alt="Screenshot">

This project was created to create a custom resume for my own needs.

<div align="right">(<a href="#top">back to top</a>)</div>

### Built With

* [React.js](https://reactjs.org/)
* [TypeScript](https://www.typescriptlang.org/)
* [Emotion](https://emotion.sh/)
* [ReactToPrint](https://github.com/gregnb/react-to-print)

<div align="right">(<a href="#top">back to top</a>)</div>

## Getting Started

To get a local copy up and running follow these simple example steps.

### Prerequisites

To run this project, you actually need to install only `npm`:

```sh
npm install npm@latest -g
```

### Installation

1. Clone the repo
   ```sh
   git clone https://gitlab.com/marekstreich/cv.git
   ```
2. Install NPM packages
   ```sh
   npm install
   ```

<div align="right">(<a href="#top">back to top</a>)</div>

## Usage

To create a resume with your own data you need to modify the contents of the `data` folder.

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app). In the project directory, you can run:

### `yarn start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

### `yarn test`

Launches the test runner in the interactive watch mode.<br />
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `yarn build`

Builds the app for production to the `build` folder.<br />
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br />
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

<div align="right">(<a href="#top">back to top</a>)</div>

## Roadmap

- Adding custom templates
- Adding knobs for toggling both templates and CV
- Fetching resume data from Linkedin

See the [open issues](https://gitlab.com/marekstreich/cv/-/issues) for a full list of proposed features (and known issues).

<div align="right">(<a href="#top">back to top</a>)</div>

## Contributing

If you have a suggestion that would make this better, please fork the repo and create a merge request. You can also simply open an issue with the tag "enhancement".
Don't forget to give the project a star! Thanks again!

1. Fork the Project
2. Create your Feature Branch (`git checkout -b my-amazing-feature`)
3. Commit your Changes using [conventional commits](https://www.conventionalcommits.org/en/v1.0.0/) (`git commit -m 'feat: my amazing feature'`)
4. Push to the Branch (`git push origin my-amazing-feature`)
5. Open a Merge Request

<div align="right">(<a href="#top">back to top</a>)</div>

## Contact

Marek Streich - [LinkedIn](https://www.linkedin.com/in/marekstreich/)

Project Link: [https://gitlab.com/marekstreich/cv](https://gitlab.com/marekstreich/cv)

<div align="right">(<a href="#top">back to top</a>)</div>
