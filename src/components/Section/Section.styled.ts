import styled from '@emotion/styled';
import { colors, fontWeights, spacing } from '../../utilities/styles';

export const Wrapper = styled.div`
	display: flex;
	flex-direction: column;
`;

export const Icon = styled.div`
	display: flex;

	svg {
		width: ${spacing.m}cm;
		width: ${spacing.m}cm;
		color: ${colors.WHITE.a900};
	}
`;

export const Title = styled.h4`
	margin: 0;
	color: ${colors.WHITE.a900};
	font-weight: ${fontWeights.bold};
	font-size: ${spacing.l}cm;
	text-transform: uppercase;
`;

export const Content = styled.div`
	margin-top: ${spacing.s}cm;
	color: ${colors.WHITE.a900};
	font-weight: ${fontWeights.regular};
	font-size: ${spacing.m}cm;
`;
