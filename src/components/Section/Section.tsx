import React, { FunctionComponent, ReactNode } from 'react';
import { Wrapper, Icon, Title, Content } from './Section.styled';

type SectionProps = {
	icon: ReactNode;
	title?: string;
};

const Section: FunctionComponent<SectionProps> = ({ children, icon, title }) => {
	return (
		<Wrapper>
			<Icon>{icon}</Icon>
			{title && <Title>{title}</Title>}
			<Content>{children}</Content>
		</Wrapper>
	);
}

export default Section;
