import styled from '@emotion/styled';
import { colors, spacing, fontWeights } from '../../utilities/styles';

export const Wrapper = styled.div`
	display: flex;
	flex-direction: column;
	justify-content: flex-start;
	align-items: flex-start;
`;

const Text = styled.h2`
	margin: 0;
	font-size: ${spacing.xl}cm;
	line-height: ${spacing.xl}cm;
	text-transform: uppercase;
	color: ${colors.WHITE.a900};
`;

export const Forename = styled(Text)`
	font-weight: ${fontWeights.regular};
`;

export const Surname = styled(Text)`
	font-weight: ${fontWeights.bold};
`;

export const Role = styled.h3`
	margin: ${spacing.xs}cm 0 0;
	padding: ${spacing.s}cm ${spacing.l}cm;
	font-size: ${spacing.m}cm;
	line-height: ${spacing.m}cm;
	font-weight: ${fontWeights.regular};
	text-transform: uppercase;
	background: ${colors.WHITE.a900};
	color: ${colors.WHITE.a025};
`;

export const Links = styled.ul`
	display: flex;
	flex-direction: row;
	justify-content: flex-start;
	align-items: center;
	list-style-type: none;
	margin: ${spacing.m}cm 0 0;
	padding: 0;
`;

export const Link = styled.a`
	display: flex;
	flex-direction: row;
	justify-content: flex-start;
	align-items: center;
	margin: 0 ${spacing.l}cm 0 0;
	color: ${colors.WHITE.a900};
	font-size: ${spacing.m}cm;
	text-decoration: none;

	&:last-of-type {
		margin-right: 0;
	}

	& svg {
		width: ${spacing.m}cm;
		height: ${spacing.m}cm;
		margin-right: ${spacing.s}cm;
		color: ${colors.WHITE.a300};
	}
`;
