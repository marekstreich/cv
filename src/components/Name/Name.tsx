import React, { FunctionComponent } from 'react';
import { FiGitlab, FiLinkedin } from 'react-icons/fi';
import { Person } from '../../types/person';
import { Wrapper, Forename, Surname, Role, Links, Link } from './Name.styled';

type NameProps = Pick<Person, 'forename' | 'surname' | 'urls'>;

const Name: FunctionComponent<NameProps> = ({ forename, surname, urls }) => {
	return (
		<Wrapper>
			<Forename>{forename}</Forename>
			<Surname>{surname}</Surname>
			<Role>Senior Frontend Developer</Role>
			<Links>
				<Link href={urls.linkedin} target="_blank">
					<FiLinkedin />
					marekstreich
				</Link>
				<Link href={urls.gitlab} target="_blank">
					<FiGitlab />
					marekstreich
				</Link>
			</Links>
		</Wrapper>
	);
}

export default Name;
