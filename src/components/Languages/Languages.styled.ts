import styled from '@emotion/styled';
import { colors, fontWeights, spacing } from '../../utilities/styles';

export const Wrapper = styled.ul`
	display: flex;
	flex-direction: column;
	justify-content: flex-start;
	align-items: flex-start;
	list-style-type: none;
	margin: 0;
	padding: 0;
`;

export const Language = styled.li`
	margin: 0 0 ${spacing.s}cm 0;
	color: ${colors.WHITE.a900};
	font-size: ${spacing.m}cm;
	line-height: ${spacing.m}cm;
	font-weight: ${fontWeights.regular};

	&:last-of-type {
		margin-bottom: 0;
	}
`;
