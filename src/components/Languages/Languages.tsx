import React, { FunctionComponent } from 'react';
import { Person } from '../../types/person';
import useLanguage from '../../utilities/hooks/useLanguage';
import { Wrapper, Language } from './Languages.styled';

type LanguagesProps = Pick<Person, 'languages'>;

const Languages: FunctionComponent<LanguagesProps> = ({ languages }) => {
	const language = useLanguage();
	return (
		<Wrapper>
			{languages.map(({ name, level }) => <Language key={name[language]}>
				{name[language]}: {level[language]}
			</Language>)}
		</Wrapper>
	);
}

export default Languages;
