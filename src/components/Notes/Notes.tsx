import React, { FunctionComponent } from 'react';
import { Text } from '@eo-locale/react';
import { Wrapper, Note } from './Notes.styled';

const Notes: FunctionComponent = () => {
	return (
		<Wrapper>
			<Note>
				<Text id="iCreatedThisResume" />
			</Note>
			<Note>
				<Text id="gdpr" />
			</Note>
		</Wrapper>
	);
}

export default Notes;
