import styled from '@emotion/styled';
import { colors, fontWeights, spacing } from '../../utilities/styles';
import { LINE_HEIGHT } from '../CV/CV.constants';

export const Wrapper = styled.div`
	display: flex;
	flex-direction: column;
	justify-content: flex-start;
	align-items: flex-start;
`;

export const Note = styled.p`
	color: ${colors.WHITE.a900};
	font-size: ${spacing.m}cm;
	line-height: ${spacing.m * LINE_HEIGHT}cm;
	font-weight: ${fontWeights.regular};
	margin: 0 0 ${spacing.xs}cm;

	&:last-of-type {
		margin-bottom: 0;
	}
`;
