import styled from '@emotion/styled';
import { colors, fontWeights, spacing } from '../../utilities/styles';

export const Wrapper = styled.div`
	display: flex;
	flex-direction: column;
	justify-content: flex-start;
	align-items: stretch;
	margin: 0 0 ${spacing.s * -1}cm;
`;

export const Set = styled.ul`
	display: flex;
	flex-direction: row;
	flex-wrap: wrap;
	justify-content: flex-start;
	align-items: flex-start;
	list-style-type: none;
	margin: 0 0 ${spacing.s}cm;
	padding: 0;

	&:last-of-type {
		margin-bottom: 0;
	}
`;

export const Skill = styled.li`
	color: ${colors.WHITE.a900};
	font-size: ${spacing.m}cm;
	line-height: ${spacing.m}cm;
	font-weight: ${fontWeights.regular};
	margin: 0 ${spacing.l}cm ${spacing.s}cm 0;
`;
