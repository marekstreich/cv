import React, { FunctionComponent } from 'react';
import { Person } from '../../types/person';
import useLanguage from '../../utilities/hooks/useLanguage';
import { Wrapper, Set, Skill } from './Skills.styled';

type SkillsProps = Pick<Person, 'skills'>;

const Skills: FunctionComponent<SkillsProps> = ({ skills }) => {
	const language = useLanguage();
	return (
		<Wrapper>
			<Set>{skills.technical.map((skill) => <Skill key={skill}>{skill}</Skill>)}</Set>
			<Set>{skills.soft[language].map((skill) => <Skill key={skill}>{skill}</Skill>)}</Set>
		</Wrapper>
	);
}

export default Skills;
