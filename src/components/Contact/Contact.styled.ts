import styled from '@emotion/styled';
import { colors, spacing, fontWeights } from '../../utilities/styles';
import { PAGE_PADDING } from '../CV/CV.constants';

export const Wrapper = styled.div`
	position: relative;
	height: ${spacing.m}cm;
	margin-bottom: ${PAGE_PADDING}cm;
`;

export const Items = styled.ul`
	position: absolute;
	height: ${spacing.m}cm;
	display: flex;
	flex-direction: row;
	list-style-type: none;
	margin: 0;
	padding: 0;
`;

export const Item = styled.li`
	display: flex;
	margin: 0 ${spacing.m * 2}cm 0 0;
	padding: 0;
	white-space: nowrap;
	font-size: ${spacing.m}cm;
	line-height: ${spacing.m}cm;

	&:last-of-type {
		margin-right: 0;
	}
`;

export const Label = styled.span`
	font-weight: ${fontWeights.bold};
	margin-right: ${spacing.m * 0.5}cm;
	color: ${colors.WHITE.a900};
`;

export const Value = styled.a`
	font-weight: ${fontWeights.regular};
	text-decoration: none;
	color: ${colors.WHITE.a900};
`;
