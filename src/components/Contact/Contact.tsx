import React, { FunctionComponent } from 'react';
import { Text } from '@eo-locale/react';
import { Person } from '../../types/person';
import { Wrapper, Items, Item, Label, Value } from './Contact.styled';

type ContactProps = Pick<Person, 'email' | 'phoneNumber'>;

const Contact: FunctionComponent<ContactProps> = ({ email, phoneNumber }) => {
	return (
		<Wrapper>
			<Items>
				<Item>
					<Label>
						<Text id="call" />
					</Label>
					<Value href={`tel:${phoneNumber}`}>{phoneNumber}</Value>
				</Item>
				<Item>
					<Text id="or" />
				</Item>
				<Item>
					<Label>
						<Text id="write" />
					</Label>
					<Value href={`mailto:${email}`}>{email}</Value>
				</Item>
			</Items>
		</Wrapper>
	);
}

export default Contact;
