import styled from '@emotion/styled';
import { colors, fontWeights, spacing } from '../../utilities/styles';
import { LINE_HEIGHT } from '../CV/CV.constants';

const List = styled.ul`
	display: flex;
	flex-direction: column;
	justify-content: flex-start;
	align-items: stretch;
	list-style-type: none;
	margin: 0;
	padding: 0;
`;

const Item = styled.li`
	display: flex;
	flex-direction: column;
	justify-content: flex-start;
	align-items: stretch;
`;

export const Wrapper = styled(List)``;

export const Experience = styled(Item)`
	margin: 0 0 ${spacing.l}cm 0;

	&:last-of-type {
		margin-bottom: 0;
	}
`;

export const Title = styled.h5`
	margin: 0;
	color: ${colors.WHITE.a900};
	font-size: ${spacing.m}cm;
	line-height: ${spacing.m * LINE_HEIGHT}cm;
	font-weight: ${fontWeights.bold};
`;

export const Roles = styled(List)`
	margin: ${spacing.s}cm 0 0;
`;

export const Role = styled(Item)`
	margin: 0 0 ${spacing.s}cm 0;
	padding: ${spacing.xs}cm 0 ${spacing.xs}cm ${spacing.s}cm;
	border-left: 0.1cm ${colors.WHITE.a200} solid;

	&:last-of-type {
		margin-bottom: 0;
	}
`;

export const Technologies = styled(List)`
	margin: ${spacing.s}cm 0 ${spacing.s * -1}cm;
	flex-wrap: wrap;
	flex-direction: row;
	align-items: flex-start;
`;

export const Technology = styled(Item)`
	justify-content: center;
	align-items: center;
	margin: 0 ${spacing.s}cm ${spacing.s}cm 0;
	padding: ${spacing.xs}cm ${spacing.s * 1.5}cm;
	border-radius: ${spacing.m}cm;
	background: ${colors.WHITE.a900};
	color: ${colors.WHITE.a025};
	font-size: ${spacing.m * 0.85}cm;
	line-height: ${spacing.m}cm;
	font-weight: ${fontWeights.bold};

	&:last-of-type {
		margin-right: 0;
	}
`;

export const Description = styled.p`
	margin: ${spacing.s}cm 0 0;
	color: ${colors.WHITE.a900};
	font-size: ${spacing.m}cm;
	line-height: ${spacing.m * LINE_HEIGHT}cm;
	font-weight: ${fontWeights.regular};
`;
