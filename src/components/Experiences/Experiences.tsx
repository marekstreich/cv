import React, { FunctionComponent } from 'react';
import { Text } from '@eo-locale/react';
import { Person } from '../../types/person';
import useLanguage from '../../utilities/hooks/useLanguage';
import { Wrapper, Experience, Title, Roles, Role, Description, Technologies, Technology } from './Experiences.styled';

type ExperiencesProps = Pick<Person, 'experiences'>;

const Experiences: FunctionComponent<ExperiencesProps> = ({ experiences }) => {
	const language = useLanguage();
	return (
		<Wrapper>
			{experiences.map((experience) => <Experience key={experience.company}>
				<Title>
					{experience.company}
				</Title>
				<Roles>
					{experience.roles.map((role) => <Role key={role.name}>
						<div>{role.name}</div>
						<div>
							{role.startDate.toLocaleDateString()}
							{' - '}
							{role.endDate ? role.endDate.toLocaleDateString() : <Text id="now" />}
						</div>
					</Role>)}
				</Roles>
				<Technologies>
					{experience.technologies.map((technology) => <Technology key={technology}>
						{technology}
					</Technology>)}
				</Technologies>
				<Description>
					<Text id={experience.description[language]} />
				</Description>
			</Experience>)}
		</Wrapper>
	);
}

export default Experiences;
