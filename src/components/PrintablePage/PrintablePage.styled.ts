import styled from '@emotion/styled';
import { colors, fontFamilies, fontWeights } from '../../utilities/styles';
import { PrintablePageProps } from './PrintablePage';

export const Wrapper = styled.div`
	display: flex;
	flex-direction: column;
	align-items: stretch;
	padding: 1rem;
	border-radius: 0.5rem;
	box-shadow: 0 0.5rem 1.5rem rgba(0, 0, 0, 0.1);
	background: linear-gradient(${colors.WHITE.a025}, ${colors.WHITE.a050});
`;

export const Header = styled.div`
	display: flex;
	flex-direction: row;
	justify-content: space-between;
	align-items: center;
	margin-bottom: 1rem;
`;

export const Trigger = styled.button`
	margin-right: 1rem;
	color: ${colors.WHITE.a025};
	background: ${colors.BASE.red};
	border: none;
	outline: none;
	padding: 0.5rem 1rem;
	border-radius: 0.25rem;
	font-size: 1rem;
	font-weight: ${fontWeights.bold};
	font-family: ${fontFamilies.mPlus};
	text-transform: uppercase;
	cursor: pointer;
	transition: background 0.2s;

	&:hover {
		background: ${colors.BASE.blue};
		transition: background 0.2s;
	}
`;

export const Title = styled.h3`
	margin: 0;
	padding: 0;
	font-size: 1rem;
	color: ${colors.BASE.red};
	font-weight: ${fontWeights.bold};
`;

export const PagesWrapper = styled.div`
	box-shadow: 0 0 0.5rem rgba(0, 0, 0, 0.1);
`;

export const Pages = styled.div<PrintablePageProps>`
	box-sizing: border-box;
	display: flex;
	flex-direction: column;
	justify-content: flex-start;
	align-items: flex-start;
	align-content: flex-start;
	flex-wrap: wrap;
	width: ${({ pageWidth }) => pageWidth};
	height: ${({ pageHeight }) => pageHeight};
	margin: 0;
	padding: 0;
	background: #fff;
`;
