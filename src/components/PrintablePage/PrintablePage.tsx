import React, { FunctionComponent, useRef } from 'react';
import { useReactToPrint } from 'react-to-print';
import { Wrapper, Header, Trigger, Title, PagesWrapper, Pages } from './PrintablePage.styled';

export type PrintablePageProps = {
	title?: string;
	pageWidth?: number;
	pageHeight?: number;
};

const PrintablePage: FunctionComponent<PrintablePageProps> = ({
	children,
	title,
	pageWidth = 21,
	pageHeight = 29.7,
}) => {
	const ref = useRef(null);
	const onClick = useReactToPrint({
		content: () => ref.current,
		documentTitle: title,
		pageStyle: `
			@media print {
				@page {
					size: ${pageWidth}cm ${pageHeight}cm;
					margin: 0cm;
					bleed: 0cm;
				}
			}
		`,
	});

	return (
		<Wrapper>
			<Header>
				<Trigger onClick={onClick}>Print</Trigger>
				{title && <Title>{title}</Title>}
			</Header>
			<PagesWrapper>
				<Pages
					ref={ref}
					pageWidth={pageWidth}
					pageHeight={pageHeight}
				>{children}</Pages>
			</PagesWrapper>
		</Wrapper>
	);
}

export default PrintablePage;