import styled from '@emotion/styled';
import { colors, fontWeights, spacing } from '../../utilities/styles';
import { LINE_HEIGHT } from '../CV/CV.constants';

export const Wrapper = styled.ul`
	display: flex;
	flex-direction: row;
	justify-content: space-between;
	align-items: flex-start;
	list-style-type: none;
	margin: 0;
	padding: 0;
`;

export const Interest = styled.li`
	display: flex;
	flex-direction: column;
	justify-content: flex-start;
	align-items: center;
	margin: 0 ${spacing.l}cm 0 0;

	&:last-of-type {
		margin-right: 0;
	}
`;

export const Icon = styled.div`
	display: flex;
	justify-content: center;
	align-items: center;
	width: ${spacing.xl}cm;
	height: ${spacing.xl}cm;
	border-radius: ${spacing.xl}cm;
	background: ${colors.WHITE.a100};

	svg {
		width: ${spacing.l}cm;
		height: ${spacing.l}cm;
		color: ${colors.WHITE.a900};
	}
`;

export const Title = styled.h5`
	margin: ${spacing.s}cm 0 0;
	color: ${colors.WHITE.a900};
	font-size: ${spacing.m}cm;
	line-height: ${spacing.m * LINE_HEIGHT}cm;
	font-weight: ${fontWeights.regular};
`;
