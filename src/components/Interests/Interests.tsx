import React, { FunctionComponent } from 'react';
import { Text } from '@eo-locale/react';
import { Person } from '../../types/person';
import useLanguage from '../../utilities/hooks/useLanguage';
import { Wrapper, Interest, Icon, Title } from './Interests.styled';

type InterestsProps = Pick<Person, 'interests'>;

const Interests: FunctionComponent<InterestsProps> = ({ interests }) => {
	const language = useLanguage();
	return (
		<Wrapper>
			{interests.map((interest) => <Interest key={interest.text.en}>
				{!!interest.icon && <Icon>
					{interest.icon}
				</Icon>}
				<Title>
					<Text id={interest.text[language]} />
				</Title>
			</Interest>)}
		</Wrapper>
	);
}

export default Interests;
