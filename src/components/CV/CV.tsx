import React, { FunctionComponent } from 'react';
import { FiBriefcase, FiGlobe, FiHeart, FiTrendingUp, FiZap } from 'react-icons/fi';
import { useTranslator } from '@eo-locale/react';
import { Person } from '../../types/person';
import Contact from '../Contact/Contact';
import Experiences from '../Experiences/Experiences';
import Interests from '../Interests/Interests';
import Languages from '../Languages/Languages';
import Name from '../Name/Name';
import Notes from '../Notes/Notes';
import Section from '../Section/Section';
import Skills from '../Skills/Skills';
import { Wrapper, LeftColumn, RightColumn } from './CV.styled';

type CVProps = {
	person: Person;
};

const CV: FunctionComponent<CVProps> = ({ person }) => {
	const { translate } = useTranslator();
	return (
		<Wrapper>
			<LeftColumn>
				<Contact {...person} />
				<Section icon={<FiBriefcase />} title={translate('experience')}>
					<Experiences {...person} />
				</Section>
			</LeftColumn>
			<RightColumn>
				<Name {...person} />
				<Section icon={<FiTrendingUp />} title={translate('skills')}>
					<Skills {...person} />
				</Section>
				<Section icon={<FiGlobe />} title={translate('languages')}>
					<Languages {...person} />
				</Section>
				<Section icon={<FiHeart />} title={translate('interests')}>
					<Interests {...person} />
				</Section>
				<Section icon={<FiZap />}>
					<Notes />
				</Section>
			</RightColumn>
		</Wrapper>
	);
}

export default CV;
