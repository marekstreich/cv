export const PAGE_WIDTH = 21;
export const PAGE_HEIGHT = 29.7;
export const PAGE_PADDING = 1.45;
export const LINE_HEIGHT = 1.25;
