import styled from '@emotion/styled';
import { colors, fontFamilies, spacing } from '../../utilities/styles';
import { PAGE_WIDTH, PAGE_HEIGHT, PAGE_PADDING } from './CV.constants';

export const Wrapper = styled.div`
	display: flex;
	flex-direction: row;
	justify-content: flex-start;
	align-items: stretch;
	width: ${PAGE_WIDTH}cm;
	height: ${PAGE_HEIGHT}cm;
	font-family: ${fontFamilies.mPlus};
`;

const Column = styled.div`
	flex: 1;
	display: flex;
	flex-direction: column;
	align-items: stretch;
`;

export const LeftColumn = styled(Column)`
	justify-content: flex-start;
	padding: ${PAGE_PADDING}cm;
	background: ${colors.WHITE.a025};
`;

export const RightColumn = styled(Column)`
	justify-content: space-between;
	padding: ${PAGE_PADDING * 2 + spacing.m}cm ${PAGE_PADDING}cm ${PAGE_PADDING}cm;
	background: ${colors.WHITE.a075};
`;