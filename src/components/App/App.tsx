import React, { FunctionComponent } from 'react';
import { TranslationsProvider } from '@eo-locale/react';
import { marek } from '../../data/marek';
import locales from '../../locales';
import { Language } from '../../types/locales';
import CV from '../CV/CV';
import PrintablePage from '../PrintablePage/PrintablePage';
import { Wrapper, Pages } from './App.styled';

const App: FunctionComponent = () => {
	return (
		<Wrapper>
			<Pages>
				<PrintablePage title="Marek Streich - Curriculum Vitae EN">
					<TranslationsProvider language={Language.English} locales={locales}>
						<CV person={marek} />
					</TranslationsProvider>
				</PrintablePage>
				<PrintablePage title="Marek Streich - Curriculum Vitae PL">
					<TranslationsProvider language={Language.Polish} locales={locales}>
						<CV person={marek} />
					</TranslationsProvider>
				</PrintablePage>
			</Pages>
		</Wrapper>
	);
}

export default App;
