import styled from '@emotion/styled';
import { fontFamilies } from '../../utilities/styles';

export const Wrapper = styled.main`
	padding: 1rem;
	display: flex;
	justify-content: center;
	align-items: center;
	font-family: ${fontFamilies.mPlus};
`;

export const Pages = styled.div`
	display: flex;
	flex-direction: row;
	flex-wrap: wrap;
	justify-content: flex-start;
	align-items: flex-start;
	margin: 0 -0.5rem;

	> * {
		margin: 0 0.5rem 0.5rem;
	}
`;
