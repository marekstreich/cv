const colors = {
	BASE: {
		blue: '#005A99',
		red: '#FF3B56',
		yellow: '#FED032',
	},
	WHITE: {
		'a025': '#FFFFFF',
		'a050': '#FDFEFE',
		'a075': '#F9FCFB',
		'a100': '#DBE1DE',
		'a200': '#C5CBC8',
		'a300': '#AFB4B2',
		'a400': '#999E9C',
		'a500': '#838786',
		'a600': '#6E716F',
		'a700': '#585A59',
		'a800': '#424443',
		'a900': '#2C2D2D',
	},
};

export default colors;