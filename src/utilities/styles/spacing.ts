const spacing = {
	xs: 0.09,
	s: 0.18,
	m: 0.36,
	l: 0.72,
	xl: 1.44,
};

export default spacing;