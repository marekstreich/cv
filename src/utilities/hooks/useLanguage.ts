import { useTranslator } from '@eo-locale/react';
import { DEFAULT_LANGUAGE, Language } from '../../types/locales';

const useLanguage = (): Language => {
	const { language } = useTranslator();
	const castedLanguage = language as Language;
	return Object.values(Language).includes(castedLanguage) ? castedLanguage : DEFAULT_LANGUAGE;
};

export default useLanguage;
