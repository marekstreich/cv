import { GiMaracas } from 'react-icons/gi';
import { FaChessKnight } from 'react-icons/fa';
import { MdFlight, MdLocalMovies } from 'react-icons/md';
import { Language } from '../types/locales';
import { Person } from '../types/person';

export const marek: Person = {
	email: 'streich.marek@gmail.com',
	experiences: [
		{
			company: 'Lastminute.com',
			description: {
				[Language.English]: "Leading a four-person team responsible for maintaining the company's core search widget and its integrations. Overseeing the development and maintenance of a large-scale content management platform. Contributing to key technical decisions related to system architecture. Recruiting and mentoring developers to strengthen the team. Organizing developer-focused events, speaking at conferences, and presenting technical solutions to international audiences.",
				[Language.Polish]: 'Kierowanie czteroosobowym zespołem. Odpowiedzialność za utrzymanie najważniejszego widżetu w firmie oraz jego integrację. Odpowiedzialność za tworzenie i utrzymywanie wielkoskalowej platformy do zarządzania trecią. Wpływanie na decyzje techniczne dotyczące architektury systemu. Rekrutowanie oraz mentorowanie innych programistów. Organizacja wydarzeń dla programistów, przemawianie na konferencjach i prezentowanie rozwiązań technicznych na międzynarodowym forum.',
			},
			roles: [
				{
					name: 'Technical Lead',
					startDate: new Date('2021-10-01'),
				},
				{
					name: 'Senior Frontend Developer',
					startDate: new Date('2020-11-01'),
					endDate: new Date('2021-09-30'),
				},
				{
					name: 'Frontend Developer',
					startDate: new Date('2016-10-01'),
					endDate: new Date('2020-10-31'),
				},
			],
			technologies: ['TypeScript', 'React', 'Redux', 'Redux-Saga', 'Emotion', 'MUI', 'React Testing Library', 'Jest', 'WebComponents', 'Polymer'],
		},
		{
			company: 'WAYN',
			description: {
				[Language.English]: 'Maintained a high-traffic social networking platform, developing both frontend and backend features. Managed a T-SQL database, including writing and optimizing stored procedures.',
				[Language.Polish]: 'Utrzymywanie serwisu społecznościowego z dużym natężeniem ruchu. Implementowanie kodu zarówno frontowego jak i backendowego. Utrzymywanie bazy danych T-SQL oraz pisanie procedur składowanych.',
			},
			roles: [
				{
					name: 'Frontend Developer',
					startDate: new Date('2012-03-08'),
					endDate: new Date('2016-09-30'),
				},
			],
			technologies: ['AngularJS', 'Bootstrap', 'jQuery', 'PrototypeJS', 'LESS', 'T-SQL', 'VB6'],
		},
		{
			company: 'Daillly',
			description: {
				[Language.English]: 'Developed a set of static landing pages. Built a PayPal payment gateway using pure JavaScript, jQuery, and PHP.',
				[Language.Polish]: 'Tworzenie zestawu statycznych stron. Programowanie bramki płatniczej do serwisu PayPal w czystym JS, jQuery oraz PHP.',
			},
			roles: [
				{
					name: 'Frontend Developer',
					startDate: new Date('2011-09-01'),
					endDate: new Date('2012-02-28'),
				},
			],
			technologies: ['JS', 'jQuery', 'CSS', 'PHP'],
		},
	],
	forename: 'Marek',
	interests: [
		{
			icon: <GiMaracas />,
			text: {
				[Language.English]: 'Salsa',
				[Language.Polish]: 'Salsa',
			}
		},
		{
			icon: <MdFlight />,
			text: {
				[Language.English]: 'Travel',
				[Language.Polish]: 'Podróżowanie',
			}
		},
		{
			icon: <MdLocalMovies />,
			text: {
				[Language.English]: 'Movies',
				[Language.Polish]: 'Filmy',
			}
		},
		{
			icon: <FaChessKnight />,
			text: {
				[Language.English]: 'Chess',
				[Language.Polish]: 'Szachy',
			}
		},
	],
	languages: [
		{
			name: {
				[Language.English]: 'Polish',
				[Language.Polish]: 'Polski',
			},
			level: {
				[Language.English]: 'Native',
				[Language.Polish]: 'Ojczysty',
			},
		},
		{
			name: {
				[Language.English]: 'English',
				[Language.Polish]: 'Angielski',
			},
			level: {
				[Language.English]: 'C1',
				[Language.Polish]: 'C1',
			},
		},
	],
	phoneNumber: '+48 607 201 970',
	skills: {
		soft: {
			[Language.English]: ['Mentoring', 'Recruitment', 'Team leadership', 'Conference speaker', 'WebDay event organizer'],
			[Language.Polish]: ['Mentorowanie', 'Rekrutacja', 'Kierowanie zespołem', 'Prelegent', 'Organizator wydarzenia WebDay'],
		},
		technical: [
			'JavaScript',
			'ES6',
			'CSS',
			'TypeScript',
			'React',
			'Redux',
			'Redux-Saga',
			'Remix',
			'Emotion',
			'Styled-components',
			'Formik',
			'MUI',
			'Jest',
			'React Testing Library',
		],
	},
	surname: 'Streich',
	urls: {
		gitlab: 'https://gitlab.com/marekstreich',
		linkedin: 'https://www.linkedin.com/in/marekstreich/',
	},
}