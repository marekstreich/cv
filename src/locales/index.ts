import { Language, Locale } from '../types/locales';
import { en, pl } from './messages';

const locales: Locale[] = [
	{
		language: Language.English,
		messages: en,
	},
	{
		language: Language.Polish,
		messages: pl,
	},
];

export default locales;