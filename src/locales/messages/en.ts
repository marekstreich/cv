import { Messages } from "../../types/locales";

const englishMessages: Messages = {
	byTheWay: 'By the way...',
	call: 'Call',
	experience: 'Experience',
	gdpr: 'I hereby give consent for my personal data to be processed for the purpose of conducting recruitment for the position for which I am applying.',
	iCreatedThisResume: 'I created this Resume fully in React. You can check its source code directly on my GitLab account.',
	interests: 'Interests',
	languages: 'Languages',
	now: 'now',
	or: 'or',
	skills: 'Skills',
	write: 'Email',
};

export default englishMessages;
