import { Messages } from "../../types/locales";

const polishMessages: Messages = {
	byTheWay: 'Przy okazji...',
	call: 'Zadzwoń',
	experience: 'Doświadczenie',
	gdpr: 'Wyrażam zgodę na przetwarzanie moich danych osobowych w celu prowadzenia rekrutacji na aplikowane przeze mnie stanowisko.',
	iCreatedThisResume: 'To CV napisałem w Reakcie. Jego kod źródłowy znajduje się na moim koncie na GitLabie.',
	interests: 'Zainteresowania',
	languages: 'Języki',
	now: 'obecnie',
	or: 'lub',
	skills: 'Umiejętności',
	write: 'Napisz',
};

export default polishMessages;
