import { ReactNode } from 'react';
import { MultilingualText, MultilingualTexts } from './locales';

type Role = {
	endDate?: Date;
	name: string;
	startDate: Date;
};

type Experience = {
	company: string;
	description: MultilingualText;
	roles: Role[];
	technologies: string[];
};

type Interest = {
	icon?: ReactNode;
	text: MultilingualText;
};

type Language = {
	name: MultilingualText;
	level: MultilingualText;
};

type Skills = {
	soft: MultilingualTexts;
	technical: string[];
};

type Urls = {
	gitlab: string;
	linkedin: string;
};

export type Person = {
	email: string;
	experiences: Experience[];
	forename: string;
	interests: Interest[];
	languages: Language[];
	phoneNumber: string;
	skills: Skills;
	surname: string;
	urls: Urls;
};
