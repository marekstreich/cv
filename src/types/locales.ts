export enum Language {
	English = 'en',
	Polish = 'pl',
};

export const DEFAULT_LANGUAGE: Language = Language.English;

export type MultilingualText = Record<Language, string>;

export type MultilingualTexts = Record<Language, string[]>;

export type Message =
	'byTheWay' |
	'call' |
	'experience' |
	'gdpr' |
	'iCreatedThisResume' |
	'interests' |
	'languages' |
	'now' |
	'or' |
	'skills' |
	'write';

export type Messages = Record<Message, string>;

export type Locale = {
	language: Language;
	messages: Messages;
};